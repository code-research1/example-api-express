class ResponseConstant {
    constructor(responseCode, responseDescription, responseTime, responseDatas) {
        this.responseCode = responseCode;
        this.responseDescription = responseDescription;
        this.responseTime = responseTime;
        this.responseDatas = responseDatas;
    }

    static paginateController(pageNumber, pageSize, totalRecordsCount, records) {
        return {
            pageNumber: pageNumber,
            pageSize: pageSize,
            totalRecordsCount: totalRecordsCount,
            records: records,
        };
    }

    static paginateRepository(records, totalRecordsCount) {
        return {
            records: records,
            totalRecordsCount: totalRecordsCount,
        };
    }
}

module.exports = ResponseConstant; 
