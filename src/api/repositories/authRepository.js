const authModel = require("../models/entity/authModel");

class AuthRepository {

    async findOne(param) {
        return await authModel.findOne({
            where: {
                username: param, 
                isActive: "ACTIVED"
            },
        });
    }

}

module.exports = new AuthRepository();