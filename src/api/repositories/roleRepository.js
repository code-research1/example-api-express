const applicationConstant = require("../../constants/applicationConstant");
const { responseConstant, paginateRepository } = require("../../constants/responseConstant");
const { Op, Sequelize } = require('sequelize');
const roleModel = require("../models/entity/roleModel");

class RoleRepository {

    async findAllRoles(req) {
        let searchCriteria = {};

        let page = parseInt(req.page, 10) || applicationConstant.FIRST_PAGE;
        let limit = parseInt(req.limit, 10) || applicationConstant.PAGINATION_MIN_LIMIT; 
        let sortDirection = req.sort_direction || applicationConstant.SORT_DESC;
        let sortBy = req.sort_by || applicationConstant.DEFAULT_SORT_COLOUMN;
        let searchBy = req.search_by;
        let search = req.search;
        let status = req.status;

        if (searchBy.length > 0 && search.length > 0) {
            if(searchBy == "role_name") {
                searchCriteria.roleName = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }
        }

        if (status.length > 0) {
            searchCriteria.isActive = status;
        }

        const result = await roleModel.findAndCountAll({ 
            where : searchCriteria,
            order: [[sortBy, sortDirection.toUpperCase()]],
            offset: (page-1) * limit, 
            limit: limit 
        });

        return paginateRepository(result.rows, result.count);
    }

    async createRoles(datas) {
        return await roleModel.create(datas);
    }

    async updateRoles(id, datas) {
        return await roleModel.update(datas, { where: { idMasterRoles: id.toString() } });
    }

    async getDataById(id) {
        return await roleModel.findByPk(id.toString());
    }

}

module.exports = new RoleRepository();