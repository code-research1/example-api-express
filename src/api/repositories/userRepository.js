const userModel = require("../models/entity/userModel");
const roleModel = require("../models/entity/roleModel");
const applicationConstant = require("../../constants/applicationConstant");
const { paginateRepository } = require("../../constants/responseConstant");

class UserRepository {

    async findAllUsers(req) {
        let searchCriteria = {};

        let page = parseInt(req.page, 10) || applicationConstant.FIRST_PAGE;
        let limit = parseInt(req.limit, 10) || applicationConstant.PAGINATION_MIN_LIMIT; 
        let sortDirection = req.sort_direction || applicationConstant.SORT_DESC;
        let sortBy = req.sort_by || applicationConstant.DEFAULT_SORT_COLOUMN;
        let searchBy = req.search_by;
        let search = req.search;
        let status = req.status;

        if (searchBy.length > 0 && search.length > 0) {
            if(searchBy == "fullname") {
                searchCriteria.fullname = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }

            if(searchBy == "address") {
                searchCriteria.address = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }

            if(searchBy == "username") {
                searchCriteria.username = {[Op.iLike]: Sequelize.fn('LOWER', `%${search}%`)};
            }
        }
        
        if (status.length > 0) {
            searchCriteria.isActive = status;
        }

        const result = await userModel.findAndCountAll(
            {
                include: [
                    {
                        model: roleModel,
                        as: 'role', 
                    }
                ],
                attributes: { exclude: ['password'] },
                where : searchCriteria,
                order: [[sortBy, sortDirection.toUpperCase()]],
                offset: (page-1) * limit, 
                limit: limit 
            }
        );

        return paginateRepository(result.rows, result.count);
    }

    async getAllDataUserByParam(param) {
        return await userModel.findAll({
            where: {
              email: param, 
            },
            attributes: { exclude: ['password'] },
        });
    }

    async createUsers(datas) {
        return await userModel.create(datas);
    }

    async updateUsers(id, datas) {
        return await userModel.update(datas, { where: { idMasterUsers: id.toString() } });
    }

    async getDataById(id) {
        return await userModel.findByPk(id.toString(), {
            attributes: { exclude: ['password', 'age'] },
          });
    }


}

module.exports = new UserRepository();