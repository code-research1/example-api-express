const express = require('express');

const authRoute = require('./authRoute');
const userRoutes = require('./userRoute');
const roleRoutes = require('./roleRoute');
const commonRoutes = require('./commonRoute');
const config = require('../../configuration/yamlConfig');

const swaggerUi = require('swagger-ui-express');
const swaggerSpec = require('../../swagger/swagger');

const router = express.Router();
  
router.get('/', (req, res) => {
	res.json("Hello, welcome to Web Service "+ config.appName);
});


router.use('/docs', swaggerUi.serve);
router.get('/docs', swaggerUi.setup(swaggerSpec));

router.use('/login', authRoute);
router.use('/user', userRoutes);
router.use('/role', roleRoutes);
router.use('/common', commonRoutes);

module.exports = router;