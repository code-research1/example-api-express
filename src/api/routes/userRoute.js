const express = require('express');
const userController = require("../controllers/userController");
const authMiddleware = require("../middlewares/authMiddleware");

const router = express.Router();


  /**
   * @swagger
   * /user:
   *   get:
   *     summary: get all exists user
   *     description: Get all exists user.
   *     tags:
   *       - User
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Resource retrieved successfully
   *       400:
   *         description: Bad request
   */
  router.get("/", authMiddleware, userController.findAll);


  /**
   * @swagger
   * /user/getAllDataUserByParam:
   *   post:
   *     summary: get all data user by param
   *     description: get all data user by param.
   *     tags:
   *       - User
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             key:
   *               type: string
   *             value:
   *               type: string
   *           example:
   *             key: This is an Example Resource
   *             value: This is an Example Resource
   *     responses:
   *       200:
   *         description: Resource created successfully
   *       400:
   *         description: Bad request
   */
  router.post("/getAllDataUserByParam", authMiddleware, userController.getAllDataUserByParam);

  /**
   * @swagger
   * /user/getDataById/{idMasterUsers}:
   *   get:
   *     summary:  get one exists user
   *     description: Get one exists user.
   *     tags:
   *       - User
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         in: path
   *         description: The unique ID of the data to retrieve.
   *         required: true
   *         schema:
   *           type: string
   *     responses:
   *       200:
   *         description: Data retrieved successfully
   *       404:
   *         description: Data not found
   */
  router.get("/getDataById/:idMasterUsers", authMiddleware, userController.getDataById);


  /**
   * @swagger
   * /user/create:
   *   post:
   *     summary: create User
   *     description: create User.
   *     tags:
   *       - User
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             idMasterRoles:
   *               type: string
   *             fullname:
   *               type: string
   *             username:
   *               type: string
   *             isGender:
   *               type: string
   *             address:
   *               type: string
   *             hpNumber:
   *               type: string
   *             email:
   *               type: string
   *             createdBy:
   *               type: string
   *           example:
   *             idMasterRoles: This is an Example Resource
   *             fullname: This is an Example Resource
   *             username: This is an example resource.
   *             isGender: This is an example resource.
   *             address: This is an example resource.
   *             hpNumber: This is an example resource.
   *             email: This is an example resource.
   *             createdBy: This is an example resource.
   *     responses:
   *       200:
   *         description: Resource created successfully
   *       400:
   *         description: Bad request
   */
  router.post("/store", authMiddleware, userController.create);


  /**
   * @swagger
   * /user/update/:idMasterUsers:
   *   put:
   *     summary: update User
   *     description: update User.
   *     tags:
   *       - User
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             idMasterUsers:
   *               type: string
   *             idMasterRoles:
   *               type: string
   *             fullname:
   *               type: string
   *             username:
   *               type: string
   *             isGender:
   *               type: string
   *             address:
   *               type: string
   *             hpNumber:
   *               type: string
   *             email:
   *               type: string
   *             updatedBy:
   *               type: string
   *           example:
   *             idMasterUsers: This is an Example Resource
   *             idMasterRoles: This is an Example Resource
   *             fullname: This is an Example Resource
   *             username: This is an example resource.
   *             isGender: This is an example resource.
   *             address: This is an example resource.
   *             hpNumber: This is an example resource.
   *             email: This is an example resource.
   *             updatedBy: This is an example resource.
   *     responses:
   *       200:
   *         description: Resource update successfully
   *       400:
   *         description: Bad request
   */
  router.put("/update/:idMasterUsers", authMiddleware, userController.update);


  /**
   * @swagger
   * /user/updateIsActive/{idMasterUsers}:
   *   put:
   *     summary: update isActive User
   *     description: update isActive User.
   *     tags:
   *       - User
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             idMasterUsers:
   *               type: string
   *             isActive:
   *               type: string
   *             updatedBy:
   *               type: string
   *           example:
   *             idMasterUsers: This is an Example Resource
   *             isActive: This is an Example Resource
   *             updatedBy: This is an example resource.
   *     responses:
   *       200:
   *         description: Resource delete successfully
   *       400:
   *         description: Bad request
   */
  router.put("/updateIsActive/:idMasterUsers", authMiddleware, userController.updateIsActive);


module.exports = router;