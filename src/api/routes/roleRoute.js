const express = require('express');
const roleController = require("../controllers/roleController");
const authMiddleware = require("../middlewares/authMiddleware");

const router = express.Router();


  /**
   * @swagger
   * /role:
   *   get:
   *     summary: get all exists role
   *     description: Get all exists role.
   *     tags:
   *       - Role
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: Resource retrieved successfully
   *       400:
   *         description: Bad request
   */
  router.get("/", authMiddleware, roleController.findAll);


  /**
   * @swagger
   * /role/getDataById/{idMasterRoles}:
   *   get:
   *     summary:  get one exists role
   *     description: Get one exists role.
   *     tags:
   *       - Role
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: id
   *         in: path
   *         description: The unique ID of the data to retrieve.
   *         required: true
   *         schema:
   *           type: string
   *     responses:
   *       200:
   *         description: Data retrieved successfully
   *       404:
   *         description: Data not found
   */
  router.get("/getDataById/:idMasterRoles", authMiddleware, roleController.getDataById);
  
  /**
   * @swagger
   * /role/create:
   *   post:
   *     summary: create Role
   *     description: create Role.
   *     tags:
   *       - Role
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             roleName:
   *               type: string
   *             description:
   *               type: string
   *             createdBy:
   *               type: string
   *           example:
   *             roleName: This is an Example Resource
   *             description: This is an Example Resource
   *             createdBy: This is an example resource.
   *     responses:
   *       200:
   *         description: Resource created successfully
   *       400:
   *         description: Bad request
   */
  router.post("/store", authMiddleware, roleController.create);
  
  
  /**
   * @swagger
   * /role/update/{idMasterRoles}:
   *   post:
   *     summary: update Role
   *     description: update Role.
   *     tags:
   *       - Role
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             idMasterRoles:
   *               type: string
   *             roleName:
   *               type: string
   *             description:
   *               type: string
   *             updatedBy:
   *               type: string
   *           example:
   *             idMasterRoles: This is an Example Resource
   *             roleName: This is an Example Resource
   *             description: This is an Example Resource
   *             updatedBy: This is an example resource.
   *     responses:
   *       200:
   *         description: Resource update successfully
   *       400:
   *         description: Bad request
   */
  router.put("/update/:idMasterRoles", authMiddleware, roleController.update);
  
  
  /**
   * @swagger
   * /role/updateIsActive/{idMasterRoles}:
   *   put:
   *     summary: update isActive Role
   *     description: update isActive Role.
   *     tags:
   *       - Role
   *     consumes:
   *       - application/json
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: resource
   *         in: body
   *         description: JSON object containing resource information.
   *         required: true
   *         schema:
   *           type: object
   *           properties:
   *             idMasterRoles:
   *               type: string
   *             isActive:
   *               type: string
   *             updatedBy:
   *               type: string
   *           example:
   *             idMasterRoles: This is an Example Resource
   *             isActive: This is an Example Resource
   *             updatedBy: This is an example resource.
   *     responses:
   *       200:
   *         description: Resource delete successfully
   *       400:
   *         description: Bad request
   */
  router.put("/updateIsActive/:idMasterRoles", authMiddleware, roleController.updateIsActive);


module.exports = router;