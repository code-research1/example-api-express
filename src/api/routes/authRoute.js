const express = require('express');
const authController = require("../controllers/authController");

const router = express.Router();


   /**
     * @swagger
     * /login:
     *   post:
     *     summary: authenticate user
     *     description: Takes a create token. Return saved JSON.
     *     tags:
     *       - Authenticate user  
     *     consumes:
     *       - application/json
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: resource
     *         in: body
     *         description: JSON object containing resource information.
     *         required: true
     *         schema:
     *           type: object
     *           properties:
     *             username:
     *               type: string
     *             password:
     *               type: string
     *           example:
     *             username: Example Resource
     *             password: This is an example resource.
     *     responses:
     *       200:
     *         description: Resource created successfully
     *       400:
     *         description: Bad request
     */
    router.post("/", authController.checkLogin);

  
module.exports = router;