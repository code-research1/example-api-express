const express = require('express');
const commonController = require("../controllers/commonController");

const router = express.Router();

  router.post("/encrypt", commonController.encrypt);
  router.post("/decrypt", commonController.decrypt);

module.exports = router;