
module.exports = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'https://example.com'); // Allow requests from 'https://example.com'
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE'); // Allow specific HTTP methods
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization'); // Allow specific headers
  
    // Handle preflight requests (OPTIONS)
    if (req.method === 'OPTIONS') {
      res.status(200).end();
    } else {
      next();
    }
};