const jwt = require('jsonwebtoken');
const asyncHandler = require('express-async-handler');

const responseConstant = require('../../constants/responseConstant');
const commonUtils = require('../../utils/commonUtils');
const stringConstant = require('../../constants/stringConstant');
const config = require('../../configuration/yamlConfig');

const environment = config.environment;

const authMiddleware = asyncHandler(async (req, res, next) => {
    const result = new responseConstant();
    const token = req.header('Authorization').split(' ')[1];
    const apiKey = req.header('Api-Key')
    const signature = req.header('Signature')
    const signatureTime = req.header('Signature-Time')

    if(apiKey != config.key[environment].apiKeyEncode) {
        result.responseCode = 401
        result.responseDescription = stringConstant.ERROR
        result.responseTime = commonUtils.formatTimestamp(Date.now())
        result.responseDatas = stringConstant.HEADER_API_KEY
        return res.status(401).json(result);
    }

    if(signature != config.key[environment].signatureKeyEncode) {
        result.responseCode = 401
        result.responseDescription = stringConstant.ERROR
        result.responseTime = commonUtils.formatTimestamp(Date.now())
        result.responseDatas = stringConstant.HEADER_SIGNATURE_KEY
        return res.status(401).json(result);
    }

    if(signatureTime <= 0) {
        result.responseCode = 401
        result.responseDescription = stringConstant.ERROR
        result.responseTime = commonUtils.formatTimestamp(Date.now())
        result.responseDatas = stringConstant.HEADER_SIGNATURE_TIME
        return res.status(401).json(result);
    }

    if (!token) {
        result.responseCode = 401
        result.responseDescription = stringConstant.ERROR
        result.responseTime = commonUtils.formatTimestamp(Date.now())
        result.responseDatas = stringConstant.UNAUTHORIZED_ACCESS
        return res.status(401).json(result);
    }

    try {
        const signature = commonUtils.genHMACSHA256(config.key[environment].signatureKey, config.key[environment].apiKey);

        const decoded = jwt.verify(token, signature);

        next();
    } catch (error) {
        if (error.name === 'TokenExpiredError') {
            result.responseCode = 401
            result.responseDatas = stringConstant.PROVIDED_TOKEN_EXPIRED
        } else if (error.name === 'JsonWebTokenError') {
            result.responseCode = 403
            result.responseDatas = stringConstant.TOKEN_DECODING
        } else {
            result.responseCode = 500
            result.responseDatas = stringConstant.INTERNAL_SERVER_ERROR
        }
        result.responseDescription = error
        result.responseTime = commonUtils.formatTimestamp(Date.now())

        return res.status(result.responseCode).json(result);
    }

});


module.exports = authMiddleware;