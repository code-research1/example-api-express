const Sequelize = require('sequelize');
const Model = Sequelize.Model;
const sequelize = require("../../../configuration/databaseConfig");

module.exports.Model = Model;
module.exports.Sequelize = Sequelize;
module.exports.sequelize = sequelize;