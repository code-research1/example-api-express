const moment = require('moment');

var tableName = 'master_users';
const Base = require('./baseModel');
class Auth extends Base.Model {}
const sequelize = Base.sequelize;
const DataTypes = Base.Sequelize;

function initialize(sequelize,DataTypes){
    return Auth.init({
        idMasterUsers: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            unique: true,
            field: 'id_master_users'
        },
        username: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'username'
        },
        password: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'password'
        },
        isActive: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'is_active'
        },
    }, {
          sequelize,
          freezeTableName: true,
          modelName: tableName,
          timestamps: false
        }
    );
}
//sequelize.sync(); 

var AuthModel = initialize(sequelize,DataTypes);

module.exports = AuthModel;
