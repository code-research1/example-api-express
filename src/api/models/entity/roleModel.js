const moment = require('moment');

var tableName = 'master_roles';
const Base = require('./baseModel');
class Role extends Base.Model {}
const sequelize = Base.sequelize;
const DataTypes = Base.Sequelize;

function initialize(sequelize,DataTypes){
    return Role.init({
        idMasterRoles: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            unique: true,
            field: 'id_master_roles'
        },
        roleName: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'role_name'
        },
        description: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'description'
        },
        createdBy: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'created_by'
        },
        updatedBy: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'updated_by'
        },
        isActive: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'is_active'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'created_at',
            get: function() {
              return moment(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'updated_at',
            get: function() {
                return moment(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
    }, {
          sequelize,
          freezeTableName: true,
          modelName: tableName,
          timestamps: false
        }
    );
}

//sequelize.sync(); 

var RoleModel = initialize(sequelize,DataTypes);


module.exports = RoleModel;
