const moment = require('moment');

var tableName = 'master_users';
const Base = require('./baseModel');
class User extends Base.Model {}
const sequelize = Base.sequelize;
const DataTypes = Base.Sequelize;

const roleModel = require("./roleModel");

function initialize(sequelize,DataTypes){
    return User.init({
        idMasterUsers: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            unique: true,
            field: 'id_master_users'
        },
        idMasterRoles: {
            type: DataTypes.STRING(40),
            defaultValue: DataTypes.UUIDV4,
            field: 'id_master_roles'
        },
        fullname: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'fullname'
        },
        username: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'username'
        },
        isGender: {
            type: DataTypes.STRING(1),
            allowNull: false,
            field: 'is_gender'
        },
        address: {
            type: DataTypes.TEXT,
            allowNull: true,
            field: 'address'
        },
        hpNumber: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'hp_number'
        },
        dateActivation: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'date_activation',
            get: function() {
                return moment(this.getDataValue('dateActivation')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        email: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'email'
        },
        emailVerifiedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'email_verified_at',
            get: function() {
                return moment(this.getDataValue('emailVerifiedAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        password: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'password'
        },
        urlPhoto: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'url_photo'
        },
        createdBy: {
            type: DataTypes.STRING(191),
            allowNull: false,
            field: 'created_by'
        },
        updatedBy: {
            type: DataTypes.STRING(191),
            allowNull: true,
            field: 'updated_by'
        },
        isActive: {
            type: DataTypes.STRING(20),
            allowNull: true,
            field: 'is_active'
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: false,
            field: 'created_at',
            get: function() {
                return moment(this.getDataValue('createdAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW,
            allowNull: true,
            field: 'updated_at',
            get: function() {
                return moment(this.getDataValue('updatedAt')).format('YYYY-MM-DD HH:mm:ss')
            }
        },
    }, {
          sequelize,
          freezeTableName: true,
          modelName: tableName,
          timestamps: false,
        },
        
    );
}


//sequelize.sync(); 

var UserModel = initialize(sequelize,DataTypes);

UserModel.belongsTo(roleModel, 
    { 
        as: 'role', 
        foreignKey: 'idMasterRoles' 
    }
);

module.exports = UserModel;
