
const crypto = require('crypto');
const responseConstant = require('../../constants/responseConstant');
const stringConstant = require('../../constants/stringConstant');
const applicationConstant = require('../../constants/applicationConstant');
const commonUtils = require('../../utils/commonUtils');


class CommonController {
    async encrypt(req, res) {
        const result = new responseConstant();
        try {
            const { data, keyType } = req.body;

            let constantKey = "";
            if(keyType === "DATA"){
                constantKey = Buffer.from(applicationConstant.KEY_AES, 'utf8').toString('hex');
            } else if(keyType === "PASSWORD"){
                constantKey = Buffer.from(applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
            } 

            const keyBuffer = Buffer.from(constantKey, 'hex');
            const iv = crypto.randomBytes(16); // 128-bit IV
            
            // Ensure data is a string (convert it if it's an object or other data type)
            const dataToEncrypt = typeof data === 'string' ? data : JSON.stringify(data);

            const cipher = crypto.createCipheriv('aes-256-cbc', keyBuffer, iv);
            
            let encryptedData = cipher.update(dataToEncrypt, 'utf8', 'hex');
            encryptedData += cipher.final('hex');
        
            // Concatenate IV and encrypted data
            const combinedData = iv.toString('hex') + encryptedData;
          
            result.responseCode = 200
            result.responseDescription = stringConstant.SUCCESS
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = combinedData
            
            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async decrypt(req, res) {
        const result = new responseConstant();
        try {
            const { encryptedData, keyType } = req.body;

            let constantKey = "";
            if(keyType === "DATA"){
                constantKey = Buffer.from(applicationConstant.KEY_AES, 'utf8').toString('hex');
            } else if(keyType === "PASSWORD"){
                constantKey = Buffer.from(applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
            } 
		
            // Extract IV and ciphertext
            const ivHex = encryptedData.slice(0, 32);
            const iv = Buffer.from(ivHex, 'hex');
            const ciphertext = encryptedData.slice(32);
        
            const keyBuffer = Buffer.from(constantKey, 'hex');
            const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
        
            let decrypted = decipher.update(ciphertext, 'hex', 'utf8');
            decrypted += decipher.final('utf8');
        
            let decryptedData;

            try {
                decryptedData = JSON.parse(decrypted);
            } catch (e) {
                decryptedData = decrypted;
            }
          
            result.responseCode = 200
            result.responseDescription = stringConstant.SUCCESS
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = decryptedData
            
            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }
}

module.exports = new CommonController();
