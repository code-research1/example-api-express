
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
const responseConstant = require('../../constants/responseConstant');
const commonUtils = require('../../utils/commonUtils');
const userRepository = require('../repositories/userRepository');
const roleRepository = require('../repositories/roleRepository');
const stringConstant = require('../../constants/stringConstant');


class UserController {
    async findAll(req, res) {
        const result = new responseConstant();
        try {
            const { page, limit } = req.query;

            const response = await userRepository.findAllUsers(req.query);
          
            result.responseCode = 200
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = responseConstant.paginateController(page, limit, response.totalRecordsCount, response.records)
           
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err
    
            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async getAllDataUserByParam(req, res) {
        const result = new responseConstant();
        const key = req.body.key;
        const value = req.body.value;

        try {
            const datas = await userRepository.getAllDataUserByParam(value);

            if (Array.isArray(datas) && datas.length === 0) {
            
                result.responseCode = 404
                result.responseDescription = stringConstant.ERROR
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
                
            } else {
            
                result.responseCode = 200
                result.responseDescription = stringConstant.ERROR
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = datas
                
            }
            
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async getDataById(req, res) {
        const result = new responseConstant();
        try {
            const datas = await userRepository.getDataById(req.params.idMasterUsers);
          
            if (datas == null) {
                result.responseCode = 404
                result.responseDescription = stringConstant.NOT_FOUND
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
          
            } else {
                result.responseCode = 200
                result.responseDescription = stringConstant.ERROR
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = datas
            }
            
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err
    
            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async create(req, res) {
        const result = new responseConstant();
        try {
            const { idMasterRoles
                , fullname
                , username
                , isGender
                , address
                , hpNumber
                , email
                , createdBy } = req.body;

            const StoreUserCriteria = {
                idMasterUsers: uuidv4(),
                idMasterRoles: idMasterRoles,
                fullname: fullname,
                username: username,
                isGender: isGender,
                address: address,
                hpNumber: hpNumber,
                password: await bcrypt.hash("p@ssw0rd", 10),
                email: email,
                createdBy: createdBy,
                isActive: 'ACTIVED',
            };
    
            const datas = await userRepository.createUsers(StoreUserCriteria);
          
            result.responseCode = 200
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = stringConstant.SUCCESSFULLY_ADD
            
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err
    
            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async update(req, res) {
        const result = new responseConstant();
        try {
            const { idMasterUsers } = req.params;
            const { idMasterRoles
                , fullname
                , username
                , isGender
                , address
                , hpNumber
                , email
                , updatedBy } = req.body;

            const UpdateUserCriteria = {
                idMasterUsers: idMasterUsers,
                idMasterRoles: idMasterRoles,
                fullname: fullname,
                username: username,
                isGender: isGender,
                address: address,
                hpNumber: hpNumber,
                email: email,
                updatedBy: updatedBy,
                isActive: 'ACTIVED',
            };

            const dataUsers = await userRepository.getDataById(idMasterUsers);
            const dataRoles = await roleRepository.getDataById(idMasterRoles);
            
            if (dataUsers == null && dataRoles == null) {
                result.responseCode = 404
                result.responseDescription = stringConstant.NOT_FOUND
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
          
            } else {
                const datas = await userRepository.updateUsers(idMasterUsers, UpdateUserCriteria);
    
                result.responseCode = 200
                result.responseDescription = stringConstant.ERROR
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.SUCCESSFULLY_UPDATE
            }
            
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err
    
            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async updateIsActive(req, res) {
        const result = new responseConstant();
        try {

            const { idMasterUsers } = req.params;
            const { isActive, updatedBy } = req.body;

            const UpdateIsActiveUserCriteria = {
                idMasterUsers: idMasterUsers,
                isActive: isActive,
                updatedBy: updatedBy,
            };

            const dataUsers = await userRepository.getDataById(idMasterUsers);
            
            if (dataUsers == null) {
                result.responseCode = 404
                result.responseDescription = stringConstant.NOT_FOUND
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
          
            } else {
                const datas = await userRepository.updateUsers(idMasterUsers, UpdateIsActiveUserCriteria);
    
                result.responseCode = 200
                result.responseDescription = stringConstant.ERROR
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.SUCCESSFULLY_DELETE
            }
            
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err
    
            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }
}

module.exports = new UserController();
