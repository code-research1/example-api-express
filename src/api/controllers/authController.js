const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const uuid = require('uuid');
const crypto = require('crypto');

const responseConstant = require('../../constants/responseConstant');
const commonUtils = require('../../utils/commonUtils');
const authRepository = require('../repositories/authRepository');
const stringConstant = require('../../constants/stringConstant');
const config = require('../../configuration/yamlConfig');
const applicationConstant = require('../../constants/applicationConstant');

const environment = config.environment;

class AuthController {
    
    async checkLogin(req, res) {
        const result = new responseConstant();

        const { username, password } = req.body;

        try {
            const datas = await authRepository.findOne(username);
          
            if (datas == null) {
            
                result.responseCode = 404
                result.responseDescription = stringConstant.SUCCESS
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
                
            } else {

                let constantKey = Buffer.from(applicationConstant.KEY_PASS_AES, 'utf8').toString('hex');
            
                // Extract IV and ciphertext
                const ivHex = password.slice(0, 32);
                const iv = Buffer.from(ivHex, 'hex');
                const ciphertext = password.slice(32);
            
                const keyBuffer = Buffer.from(constantKey, 'hex');
                const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);
            
                let decrypted = decipher.update(ciphertext, 'hex', 'utf8');
                decrypted += decipher.final('utf8');
            
                let decryptedPass;

                try {
                    decryptedPass = JSON.parse(decrypted);
                } catch (e) {
                    decryptedPass = decrypted;
                }

                const passwordIsValid = await bcrypt.compareSync(decryptedPass, datas.password);

                if (!passwordIsValid) {
                    result.responseCode = 401
                    result.responseDescription = stringConstant.ERROR
                    result.responseTime = commonUtils.formatTimestamp(Date.now())
                    result.responseDatas = stringConstant.INVALID_PASSWORD

                    commonUtils.createLog(
                        stringConstant.ERROR,
                        stringConstant.ERROR,
                        stringConstant.INVALID_PASSWORD,
                        401,
                        req.originalUrl,
                        commonUtils.getLineNumber(),
                        result,
                    );

                    return res.status(401).send(result);
                }
                
                const payload = {
                    iss: config.key[environment].apiKeyEncode,
                    iat: Math.floor(Date.now() / 1000),
                    exp: Math.floor(Date.now() / 1000) +  86400 * 5, 
                    authorized: true,
                    jti: uuid.v4(), 
                };
                
                const signatureKey = Buffer.from(config.key[environment].signatureKeyEncode, 'base64').toString('utf-8');
                const apiKey = Buffer.from(config.key[environment].apiKeyEncode, 'base64').toString('utf-8');

                const signature = commonUtils.genHMACSHA256(signatureKey, apiKey);

                const token = jwt.sign(payload,
                    signature,
                    {
                     algorithm: 'HS256',
                    });
                  
                
                result.responseCode = 200
                result.responseDescription = stringConstant.SUCCESS
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = token
                
            }
            
            res.status(200).json(result);
        } catch (err) {
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );

            res.status(500).send(result);
        }
    }
}

module.exports = new AuthController();
