const { v4: uuidv4 } = require('uuid');

const responseConstant = require('../../constants/responseConstant');
const stringConstant = require('../../constants/stringConstant');

const commonUtils = require('../../utils/commonUtils');
const roleRepository = require('../repositories/roleRepository');

class RoleController {
    async findAll(req, res) {
        const result = new responseConstant();
        try {
            const { page, limit } = req.query;

            const response = await roleRepository.findAllRoles(req.query);
            
            result.responseCode = 200
            result.responseDescription = stringConstant.SUCCESS
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            
            result.responseDatas = responseConstant.paginateController(page, limit, response.totalRecordsCount, response.records)
            
            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async getDataById(req, res) {
        const result = new responseConstant();
        try {
            const datas = await roleRepository.getDataById(req.params.idMasterRoles);
          
            if (datas == null) {
                result.responseCode = 404
                result.responseDescription = stringConstant.NOT_FOUND
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
            } else {
                result.responseCode = 200
                result.responseDescription = stringConstant.SUCCESS
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = datas
            }
            
            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }
    
    async create(req, res) {
        const result = new responseConstant();
        try {
            const { roleName, description, createdBy } = req.body;

            const StoreRoleCriteria = {
                idMasterRoles: uuidv4(),
                roleName: roleName,
                description: description,
                createdBy: createdBy,
                isActive: 'ACTIVED',
            };

            const datas = await roleRepository.createRoles(StoreRoleCriteria);
          
                result.responseCode = 200
                result.responseDescription = stringConstant.SUCCESS
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.SUCCESSFULLY_ADD
            
            
            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }
    
    async update(req, res) {
        const result = new responseConstant();
        try {
            const { idMasterRoles } = req.params;
            const { roleName, description, updatedBy } = req.body;

            const UpdateRoleCriteria = {
                idMasterRoles: idMasterRoles,
                roleName: roleName,
                description: description,
                updatedBy: updatedBy,
            };
            const datas = await roleRepository.getDataById(idMasterRoles);

            if (datas == null) {
                result.responseCode = 404
                result.responseDescription = stringConstant.NOT_FOUND
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
          
            } else {
                const datas = await roleRepository.updateRoles(idMasterRoles, UpdateRoleCriteria);
          
                result.responseCode = 200
                result.responseDescription = stringConstant.SUCCESS
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.SUCCESSFULLY_UPDATE
            
            }

            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }

    async updateIsActive(req, res) {
        const result = new responseConstant();
        try {
            const { idMasterRoles } = req.params;
            const { isActive, updatedBy } = req.body;

            const UpdateIsActiveRoleCriteria = {
                idMasterRoles: idMasterRoles,
                isActive: isActive,
                updatedBy: updatedBy,
            };

            const datas = await roleRepository.getDataById(idMasterRoles);

            if (datas == null) {
                result.responseCode = 404
                result.responseDescription = stringConstant.NOT_FOUND
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.DATA_NOT_FOUND
          
            } else {
                const datas = await roleRepository.updateRoles(idMasterRoles, UpdateIsActiveRoleCriteria);
          
                result.responseCode = 200
                result.responseDescription = stringConstant.SUCCESS
                result.responseTime = commonUtils.formatTimestamp(Date.now())
                result.responseDatas = stringConstant.SUCCESSFULLY_DELETE
            
            }

            res.status(200).json(result);
        } catch (err) {
            
            result.responseCode = 500
            result.responseDescription = stringConstant.ERROR
            result.responseTime = commonUtils.formatTimestamp(Date.now())
            result.responseDatas = err

            commonUtils.createLog(
                stringConstant.ERROR,
                stringConstant.ERROR,
                stringConstant.INTERNAL_SERVER_ERROR,
                500,
                req.originalUrl,
                commonUtils.getLineNumber(),
                result,
            );
            res.status(500).send(result);
        }
    }
}

module.exports = new RoleController();
