const crypto = require('crypto');
const applicationConstant = require('../constants/applicationConstant');
const stringConstant = require('../constants/stringConstant');
const logger = require('../utils/loggerUtils')(__filename);

const constantKey = Buffer.from(applicationConstant.KEY_AES, 'utf8').toString('hex');

function formatTimestamp(timestampInMillis) {
    const date = new Date(timestampInMillis);
  
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    const hours = String(date.getHours()).padStart(2, '0');
    const minutes = String(date.getMinutes()).padStart(2, '0');
    const seconds = String(date.getSeconds()).padStart(2, '0');
  
    return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

function genHMACSHA256(message, secretKey) {
    const hmac = crypto.createHmac('sha256', secretKey);
    hmac.update(message);
    return hmac.digest('hex');
}

function createLog(logFlagging, title, message, code, endpoint, linenumber, datas) {
    
    if (logFlagging === stringConstant.SUCCESS) {
        logger.success({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    } else if (logFlagging === stringConstant.INFO) {
        logger.info({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    } else if (logFlagging === stringConstant.ERROR) {
        logger.error({
            message: message,
            title: title,
            code: code,
            endpoint: endpoint,
            linenumber: linenumber,
            datas: datas,
        });
    } else {
        logger.error('logFlagging No match found');
    }
}

function getLineNumber() {
    // Create an error object to capture the call stack
    const error = new Error();
  
    // Split the stack trace to extract the line number
    const stackLine = error.stack.split('\n')[2]; // Adjust the index based on your code structure
  
    if (stackLine) {
      // Extract the line number from the stack line
      const lineNumber = stackLine.match(/:(\d+):\d+/);
      if (lineNumber) {
        return lineNumber[1];
      }
    }
  
    return 'Line number not found';
}

// Function to encrypt data using AES-256-CBC
function encrypt(text) {

    const keyBuffer = Buffer.from(constantKey, 'hex');
    const iv = crypto.randomBytes(16); // 128-bit IV
    const cipher = crypto.createCipheriv('aes-256-cbc', keyBuffer, iv);

    let encryptedData = cipher.update(text, 'utf8', 'hex');
    encryptedData += cipher.final('hex');

    const combinedData = iv.toString('hex') + encryptedData;

	return combinedData;
}

// Function to decrypt data using AES-256-CBC
function decrypt(encryptedText) {
    // Extract IV and ciphertext
    const ivHex = encryptedText.slice(0, 32);
    const iv = Buffer.from(ivHex, 'hex');
    const ciphertext = encryptedText.slice(32);

    const keyBuffer = Buffer.from(constantKey, 'hex');
    const decipher = crypto.createDecipheriv('aes-256-cbc', keyBuffer, iv);

    let decryptedData = decipher.update(ciphertext, 'hex', 'utf8');
    decryptedData += decipher.final('utf8');

	return decryptedData;
}

module.exports = {
    formatTimestamp,
    genHMACSHA256,
    createLog,
    getLineNumber,
    encrypt,
    decrypt
};