const { createLogger, format, transports } = require('winston');

require('winston-daily-rotate-file');

const config = require('../configuration/yamlConfig');
const environment = config.environment;

// Define a custom log format
const customFormat = format.printf(({ level, timestamp, message, title, code, endpoint, linenumber, datas }) => {
	return JSON.stringify({
	  'log.level': level,
	  '@timestamp': timestamp,
	  message,
	  title,
	  code,
	  endpoint,
	  linenumber,
	  datas,
	});
});

const options = (prefix) => ({
	format: format.combine(
		format.timestamp({ format: 'YYYY-MM-DDTHH:mm:ssZ' }),
		customFormat
	),
	transports: [
		new transports.Console(),
		new transports.DailyRotateFile({
			filename: config.logDirectory[environment].path,
			datePattern: 'DD-MMM-YYYY',
			format: format.combine(format.uncolorize()),
		}),
	],
});

// prettier-disable
const logger = (prefix) => createLogger(options(prefix));

module.exports = logger;
