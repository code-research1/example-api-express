const swaggerJSDoc = require('swagger-jsdoc');
const swaggerDefinition = require('./swaggerDefinition');

const options = {
  swaggerDefinition,
  apis: ["./src/api/routes/*.js"], // Replace with the path to your Express application file.
};

const swaggerSpec = swaggerJSDoc(options);

module.exports = swaggerSpec;