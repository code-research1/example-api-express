
module.exports = {
    info: {
        title: 'Express API Clean Architecture',
        version: '1.0.0',
        description: 'API documentation for Express application',
        termsOfService: 'https://www.example.com/terms-of-service',
        contact: {
          name: 'API Support', // Replace with the name of the contact person or entity
          email: 'support@swagger.io', // Include email address if applicable
          url: 'http://www.apache.org/licenses/LICENSE-2.0.html', // Include a URL for contact information if applicable
        },
    },
    basePath: '/dev/api/v1/example-api-express',
    schemes: ['http', 'https'], 
    securityDefinitions: {
        Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header',
            description: 'Provide a Bearer token to authenticate your requests.',
        },
    },
};