const app = require('./configuration/expressConfig');
const commonUtils = require('./utils/commonUtils');
const stringConstant = require('./constants/stringConstant');

const config = require('./configuration/yamlConfig');

app.use((req, res, next) => {
	res.status(404).json({ error: 'Ooops Page Not Found' });
});
  
app.listen(config.serverPort, (err) => {

	if (err) {
		return commonUtils.createLog(
			stringConstant.ERROR,
			stringConstant.ERROR,
			"server failed to start",
			500,
			null,
			err.lineNumber,
			err,
		);
	}
	return commonUtils.createLog(
		stringConstant.INFO,
		stringConstant.INFO,
		`Server is running on port ${config.serverPort}`,
		200,
		null,
		0,
		null,
	);
});

module.exports = app;