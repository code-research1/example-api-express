const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');


try {
    const configFilePath = path.resolve('./src/', 'config.yml');
    const config = yaml.load(fs.readFileSync(configFilePath, 'utf8'));

    module.exports = config;
} catch (e) {
    console.error('Error loading configuration:', e);
    module.exports = {};
}