const express = require('express');
const compress = require('compression');
const helmet = require('helmet');
const morgan = require('morgan');
const tmp = require('tmp');

// const { logs } = require('../constants');
const routes = require('../api/routes/mainRoute');
const corsMiddleware = require('../api/middlewares/corsMiddleware');
const config = require('../configuration/yamlConfig');

const app = express();
// gzip compression
// app.use(morgan(logs));


// Use the custom CORS middleware
app.use(corsMiddleware);

// secure apps by setting various HTTP headers
app.use(helmet());

// parse body params and attache them to req.body
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(compress());

// mount api routes
const environment = config.environment;	
app.use(config.route[environment].name, routes);


// temporary files created using tmp will be deleted on UncaughtException
tmp.setGracefulCleanup();

module.exports = app;