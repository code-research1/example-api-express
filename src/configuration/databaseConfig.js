const Sequelize = require("sequelize");
const config = require('../configuration/yamlConfig');
const stringConstant = require('../constants/stringConstant');
const commonUtils = require('../utils/commonUtils');

const environment = config.environment;

const sequelize = new Sequelize(commonUtils.decrypt(config.database[environment].connection)
                                , commonUtils.decrypt(config.database[environment].username)
                                , commonUtils.decrypt(config.database[environment].password), {
    host: commonUtils.decrypt(config.database[environment].url),
    dialect: commonUtils.decrypt(config.database[environment].connection),
    schema: commonUtils.decrypt(config.database[environment].schema),
    port: commonUtils.decrypt(config.database[environment].port),
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
});

async function checkDatabaseConnection() {
  try {
        await sequelize.authenticate();
        commonUtils.createLog(
            stringConstant.INFO,
            stringConstant.INFO,
            "Connection has been established successfully.",
            200,
            null,
            0,
            null,
        );
  } catch (error) {
        commonUtils.createLog(
            stringConstant.ERROR,
            stringConstant.ERROR,
            "Unable to connect to the database ",
            500,
            null,
            commonUtils.getLineNumber(),
            error,
        );
  }
}
checkDatabaseConnection();


module.exports = sequelize;
